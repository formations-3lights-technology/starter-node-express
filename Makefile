SHELL := /bin/bash
.SHELLFLAGS = -e -c
.SILENT:
.ONESHELL:

.EXPORT_ALL_VARIABLES:
ROOT_DIR := $(dir $(realpath $(firstword $(MAKEFILE_LIST))))

.DEFAULT_GOAL: help

.PHONY: help
help:
	@echo "Please use 'make <target>' where <target> is one of"
	@grep -E '^\.PHONY: [a-zA-Z_-]+ .*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = "(: |##)"}; {printf "\033[36m%-30s\033[0m %s\n", $$2, $$3}'

nvm:
	. ${NVM_DIR}/nvm.sh && nvm use && $(CMD)

.PHONY: install ## ⚙ Installation des packages du projet
install:
	make nvm CMD="yarn"

.PHONY: build-watch ## 🔨👀 Build en mode watch le projet
build-watch:
	yarn tsc -w -p .

.PHONY: start-dep ## ▶ Lancer les dépendances
start-dep:
	docker compose down
	sleep 2
	docker compose up -d --remove-orphans

.PHONY: stop-dep ## ⏸ Arrêter les dépendances
stop-dep:
	docker compose down

.PHONY: start ## 🎮 Lancer le projet en local
start:
	make nvm CMD="make build-watch & nodemon --exec ts-node -r tsconfig-paths/register ./build/src/start.js --delay 1"

.PHONY: fix ## 🛠 Fix les erreurs de lint du projet
fix:
	make nvm CMD="yarn gts fix"

.PHONY: test ## ✅ Lance les tests Mocha du projet
test:
	make nvm CMD="yarn mocha"

.PHONY:
test-unit:
	make nvm CMD="yarn mocha --config configuration/mocha-unit.config.js"

.PHONY: build-clean ## 🧹 Supprime le répertoire build
build-clean:
	rm -rf build

.PHONY: kill-node ## 💀 Kill Node processus
kill-node:
	pkill -f node

.PHONY: docker-clean ## 🧹 Prune de Docker
docker-clean:
	docker system prune --all

.PHONY: docker-clean-volume ## 🛑🛑🛑 🧹 Prune tous les volumes Docker 🛑🛑🛑
docker-clean-volume:
	docker system prune --all --volumes

.PHONY: full-reinstall ## 🧹⚙ Nettoyage de tout le projet (sans Docker) et réinstallation
full-reinstall:
	rm -rf node_modules \
	& make build-clean
	make install
