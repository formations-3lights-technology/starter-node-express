require('reflect-metadata');
const dotenv = require('dotenv-defaults');
const fs = require('fs');

const envConfig = dotenv.parse(fs.readFileSync('./.env.test'), './.env.defaults');
for (const k in envConfig) {
  process.env[k] = envConfig[k];
}
