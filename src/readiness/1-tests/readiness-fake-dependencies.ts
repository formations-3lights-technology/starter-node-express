import { ReadinessDependenciesContainer } from '@/readiness/2-configuration/root-readiness-dependencies-container';
import sinon from 'sinon';

export const readinessFakeDependencies = (): ReadinessDependenciesContainer => ({
  // @ts-ignore
  readyCheck: {
    handle: sinon.stub()
  }
});
