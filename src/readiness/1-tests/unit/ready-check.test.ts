import { expect } from '@/1-tests/utils';
import { ReadyCheck } from '@/readiness/use-cases/ready-check';

describe('Unit | Readiness | Ready check', () => {
  let readyCheck: ReadyCheck;

  beforeEach(() => {
    readyCheck = new ReadyCheck();
  });

  describe('Tout est ok', () => {
    it('api est ok', async () => {
      // WHEN
      const result = await readyCheck.handle();

      // THEN
      expect(result.isUp).to.be.true();
    });
  });
});
