import supertest from 'supertest';
import packageJson from '../../../../../package.json';
import { ReadyGlobalStatus } from '@/readiness/domain/ready-global-status';
import { ReadyStatus } from '@/readiness/domain/ready-status';
import { expect, fakeDependencies, FakeExpressServer } from '@/1-tests/utils';

describe('Integration | Readiness | GET /ready', () => {
  let fakeApi: supertest.SuperTest<supertest.Test>;
  let query;
  const baseUrl = '/v1/ready';

  beforeEach(async () => {
    const dependencies = fakeDependencies();
    query = dependencies.readinessDependenciesContainer.readyCheck;
    const fakeServer = await new FakeExpressServer(dependencies).create();
    fakeApi = fakeServer.api;
  });

  it('Retourne 200', () => {
    // GIVEN
    // @ts-ignore
    query.handle.resolves(new ReadyGlobalStatus());

    // WHEN
    return fakeApi
      .get(baseUrl)
      .expect(200)
      .then((response) => {
        // THEN
        expect(response.body).to.eql({
          data: {
            apiVersion: packageJson.version,
            apiStatus: ReadyStatus.UP
          }
        });
      });
  });
});
