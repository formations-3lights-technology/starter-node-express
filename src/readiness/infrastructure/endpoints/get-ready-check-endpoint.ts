import { Request, RequestHandler, Response } from 'express';
import { ReadyGlobalAllStatus, ReadyGlobalStatus } from '@/readiness/domain/ready-global-status';
import { ReadyCheck } from '@/readiness/use-cases/ready-check';

export const getReadyCheck = (query: ReadyCheck): RequestHandler => {
  return async (request: Request, response: Response) => {
    const result = await query.handle();
    const viewModel = new ReadyCheckResponseViewModel(result);
    response.status(viewModel.statusCode).send({ data: viewModel.data });
  };
};

export class ReadyCheckResponseViewModel {
  private readonly OK_CODE = 200;
  private readonly SERVICE_UNAVAILABLE_CODE = 503;

  constructor(private readonly apiGlobalStatus: ReadyGlobalStatus) {}

  get statusCode(): number {
    return this.apiGlobalStatus.isUp ? this.OK_CODE : this.SERVICE_UNAVAILABLE_CODE;
  }

  get data(): ReadyGlobalAllStatus {
    return this.apiGlobalStatus.status;
  }
}
