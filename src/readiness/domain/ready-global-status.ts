import packageJson from '../../../package.json';
import { ReadyStatus } from './ready-status';

export type ReadyGlobalAllStatus = Readonly<{
  apiVersion: string;
  apiStatus: string;
}>;

export class ReadyGlobalStatus {
  constructor() {}

  get status(): ReadyGlobalAllStatus {
    return {
      apiVersion: packageJson.version,
      apiStatus: ReadyStatus.UP
    };
  }

  get isUp(): boolean {
    return true;
  }
}
