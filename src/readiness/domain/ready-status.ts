enum ReadyStatuses {
  UP = 'up'
}

export abstract class ReadyStatus {
  static readonly UP = ReadyStatuses.UP;
}
