import { ReadyGlobalStatus } from '@/readiness/domain/ready-global-status';

export class ReadyCheck {
  constructor() {}

  async handle(): Promise<ReadyGlobalStatus> {
    return new ReadyGlobalStatus();
  }
}
