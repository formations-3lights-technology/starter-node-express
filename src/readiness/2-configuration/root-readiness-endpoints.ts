import { Router } from 'express';
import asyncHandler from 'express-async-handler';
import { getReadyCheck } from '@/readiness/infrastructure/endpoints/get-ready-check-endpoint';
import { ReadinessDependenciesContainer } from './root-readiness-dependencies-container';

export interface RootEndpoints {
  unprotected: (router: Router, dependencies: ReadinessDependenciesContainer) => void;
}

export const rootReadinessEndpoints: RootEndpoints = {
  unprotected: (router: Router, { readyCheck }: ReadinessDependenciesContainer): void => {
    router.get('/ready', asyncHandler(getReadyCheck(readyCheck)));
  }
};
