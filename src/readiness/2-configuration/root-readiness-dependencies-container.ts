import { ReadyCheck } from '@/readiness/use-cases/ready-check';

export type ReadinessDependenciesContainer = Readonly<{
  readyCheck: ReadyCheck;
}>;

export const rootReadinessDependenciesContainer = (): ReadinessDependenciesContainer => {
  return {
    readyCheck: new ReadyCheck()
  };
};
