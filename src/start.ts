import dotenv from 'dotenv-defaults';
import 'reflect-metadata';
import { dependenciesContainer } from './2-configuration/dependencies-container';
import { expressMiddlewares } from './2-configuration/express-middlewares';
import { ExpressServer } from './express-server';

dotenv.config();

const dependencies = dependenciesContainer();

const expressServer = new ExpressServer(dependencies, expressMiddlewares).create();
const server = expressServer.listen(expressServer.get('port'), () => {
  dependencies.apiLogger.info(
    `App is running at http://localhost:${expressServer.get('port')} in ${expressServer.get('env')} mode`
  );
  dependencies.apiLogger.info('Press CTRL-C to stop');
});

process.on('SIGTERM', () => {
  server.close((err) => {
    if (err) {
      dependencies.apiLogger.error(err.toString());
      process.exit(1);
    }

    process.exit(0);
  });
});
