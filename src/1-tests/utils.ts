import { StubbedType, stubInterface } from '@salesforce/ts-sinon';
import chai, { expect as chaiExpect } from 'chai';
import chaiAsPromised from 'chai-as-promised';
import deepEqualInAnyOrder from 'deep-equal-in-any-order';
import dirtyChai from 'dirty-chai';
import sinon from 'sinon';
import sinonChai from 'sinon-chai';
import superAgentDefaults from 'superagent-defaults';
import supertest from 'supertest';
import { Dependencies } from '@/2-configuration/dependencies-container';
import { ExpressMiddlewares, expressMiddlewares } from '@/2-configuration/express-middlewares';
import { ExpressServer } from '@/express-server';
import { readinessFakeDependencies } from '@/readiness/1-tests/readiness-fake-dependencies';
import { ApiLogger } from '@/shared/domain/gateways/Api-logger';
import { DateProvider } from '@/shared/domain/gateways/Date-provider';
import { NodeEnvironmentVariables } from '@/shared/infrastructure/gateways/node-environment-variables';

chai.use(sinonChai);
chai.use(dirtyChai);
chai.use(deepEqualInAnyOrder);
chai.use(chaiAsPromised);

export const fakeDependencies = (dependencies: Partial<Dependencies> = {}): Dependencies => {
  sinon.restore();
  // @ts-ignore
  return {
    environmentVariables: new NodeEnvironmentVariables(),
    apiLogger: fakeApiLogger(),
    dateProvider: fakeDateProvider(),

    readinessDependenciesContainer: readinessFakeDependencies(),
    ...dependencies
  };
};

export const fakeApiLogger: () => StubbedType<ApiLogger> = () =>
  stubInterface<ApiLogger>(sinon, { loggerOptions: undefined });

export const fakeDateProvider = (): StubbedType<DateProvider> => stubInterface<DateProvider>(sinon);

export const fakeExpressMiddlewares = (): ExpressMiddlewares => {
  return {
    ...expressMiddlewares
  };
};

export class FakeExpressServer {
  constructor(private dependencies: Dependencies) {}

  async create() {
    const server = new ExpressServer(this.dependencies, fakeExpressMiddlewares()).create();

    const fakeApi = superAgentDefaults(supertest(server));
    fakeApi.set('Authorization', 'Basic YmFja29mZmljZTpCQVNJQ19BVVRIX0FQSV9CQUNLT0ZGSUNFX1BBU1NXT1JE');
    return { server, api: fakeApi };
  }
}

export const expect = chaiExpect;
