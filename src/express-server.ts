import cors from 'cors';
import express, { Express } from 'express';
import { Dependencies } from './2-configuration/dependencies-container';
import { ExpressMiddlewares } from './2-configuration/express-middlewares';
import { v1Router } from './2-configuration/routers/v1-router';
import { rootSharedEndpoints } from './shared/2-configuration/root-shared-endpoints';

export class ExpressServer {
  constructor(private readonly dependencies: Dependencies, private readonly middlewares: ExpressMiddlewares) {}

  create(): Express {
    const server = express();

    server.disable('x-powered-by');
    server.set('port', this.dependencies.environmentVariables.API_PORT);
    server.use(cors());
    server.use(express.json());
    server.use(express.urlencoded({ extended: true }));

    rootSharedEndpoints(server);

    server.use('/v1', v1Router(this.dependencies));

    server.use(this.middlewares.postErrorHandling(this.dependencies));

    return server;
  }
}
