export class NonAutorisation extends Error {
  constructor(readonly message: string) {
    super(message);
  }
}
