import { ElementEnConflit } from './element-en-conflit';

export class OptimisticConcurrency extends ElementEnConflit {
  constructor(private nom: string, private version: number) {
    super(`${nom}: the version ${version} is not the current version`);
  }
}
