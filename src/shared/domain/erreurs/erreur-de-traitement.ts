export class ErreurDeTraitement extends Error {
  constructor(readonly message: string) {
    super(message);
  }
}
