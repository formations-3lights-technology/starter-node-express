import { Message, SerializedMessage } from '@/shared/domain/message';

export type SerializedEventMetadata = Readonly<{
  version: string;
  aggregate: string;
  aggregate_id: string;
}>;

export abstract class DomainEvent<D = {}> extends Message<SerializedEventMetadata, D> {
  protected abstract aggregate: string;

  protected constructor(private aggregateId: string) {
    super('event');
  }

  abstract label(): string;

  abstract version(): string;

  abstract isPublic(): boolean;

  serialize(): SerializedMessage<SerializedEventMetadata, D> {
    return {
      metadata: {
        id: this._messageId,
        version: this.version(),
        type: this.messageType,
        label: this.label(),
        timestamp: this._timestamp,
        bounded_context: this.boundedContext,
        aggregate: this.aggregate,
        aggregate_id: this.aggregateId
      },
      data: this.serializedData()
    };
  }

  protected abstract serializedData(): D;
}
