export interface ApiLogger {
  readonly loggerOptions;

  info(message: string, ...args: any[]): void;

  error(message: string, ...args: any[]): void;

  infoWithMetadata(metaData: any, message: string, ...args: any[]): void;

  errorWithMetadata(metaData: any, message: string, ...args: any[]): void;
}
