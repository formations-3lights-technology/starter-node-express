type SerializedMessageMetadata = Readonly<{
  id: string;
  type: string;
  label: string;
  timestamp: string;
  bounded_context: string;
}>;
export type SerializedMessage<M, D> = Readonly<{
  metadata: SerializedMessageMetadata & M;
  data: D;
}>;
export type MessageType = 'command' | 'event';

export abstract class Message<M = SerializedMessageMetadata, D = any> {
  protected abstract boundedContext: string;
  protected _messageId = '';
  protected _timestamp = '';

  protected constructor(protected messageType: MessageType) {}

  set messageId(value: string) {
    this._messageId = value;
  }

  set timestamp(value: string) {
    this._timestamp = value;
  }

  abstract label(): string;

  abstract isPublic(): boolean;

  abstract serialize(): SerializedMessage<M, D>;
}
