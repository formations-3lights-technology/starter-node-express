import pino from 'pino';
import { ApiLogger } from '@/shared/domain/gateways/Api-logger';
import { EnvironmentVariables } from '@/shared/domain/gateways/Environment-variables';

export class PinoApiLogger implements ApiLogger {
  readonly loggerOptions: pino.LoggerOptions = {
    redact: {
      paths: ['req.headers.authorization'],
      remove: true
    },
    level: this.environmentVariables.LOG_LEVEL || 'info'
  };
  private readonly pino: pino.Logger;

  constructor(private environmentVariables: EnvironmentVariables) {
    const baseOptions: pino.LoggerOptions = {
      ...this.loggerOptions,
      transport: {
        target: 'pino-pretty',
        options: {
          colorize: true,
          singleLine: true
        }
      }
    };
    if (environmentVariables.isProduction) {
      delete baseOptions.transport;
    }
    this.pino = pino(baseOptions);
  }

  info(message: string, data: any): void {
    this.pino.info(this.stringifyMessage(`[INFO]: ${message}`, data));
  }

  error(message: string, data: any): void {
    this.pino.error(this.stringifyMessage(`[ERROR]: ${message}`, data));
  }

  infoWithMetadata(base: any, message: string, data: any): void {
    this.pino.info(base, this.stringifyMessage(`[INFO]: ${message}`, data).trim());
  }

  errorWithMetadata(base: any, message: string, data: any): void {
    this.pino.error(base, this.stringifyMessage(`[ERROR]: ${message}`, data).trim());
  }

  private stringifyMessage(message: string, data: any): string {
    return JSON.stringify({ label: message, data });
  }
}
