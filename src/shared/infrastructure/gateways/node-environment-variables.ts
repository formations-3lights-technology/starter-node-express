import { EnvironmentVariables } from '@/shared/domain/gateways/Environment-variables';

export class NodeEnvironmentVariables implements EnvironmentVariables {
  readonly NODE_ENV = this.getOrError('NODE_ENV');
  readonly SQL_LOG = this.getOrError('SQL_LOG') === 'true';
  readonly LOG_LEVEL = this.getOrError('LOG_LEVEL');

  readonly API_PORT = Number(this.getOrError('API_PORT'));

  readonly DATABASE_HOST = this.getOrError('DATABASE_HOST');
  readonly DATABASE_PORT = Number(this.getOrError('DATABASE_PORT'));
  readonly DATABASE_NAME = this.getOrError('DATABASE_NAME');
  readonly DATABASE_USERNAME = this.getOrError('DATABASE_USERNAME');
  readonly DATABASE_USER_PASSWORD = this.getOrError('DATABASE_USER_PASSWORD');
  readonly DATABASE_MAX_POOL_SIZE = Number(this.getOrError('DATABASE_MAX_POOL_SIZE'));
  readonly DATABASE_SSL = this.getOrDefault('DATABASE_SSL', 'true') === 'true';
  readonly DATABASE_CONNECTION_TIMEOUT_IN_MS = Number(this.getOrError('DATABASE_CONNECTION_TIMEOUT_IN_MS'));

  get isProduction(): boolean {
    return this.NODE_ENV === 'production';
  }

  private getOrError(value: string): string {
    if (process.env[value]) {
      // @ts-ignore
      return process.env[value];
    }
    throw Error(`${value} doesn't exist`);
  }

  private getOrDefault(value: string, defaultValue: string): string {
    if (process.env[value]) {
      // @ts-ignore
      return process.env[value];
    }
    return defaultValue;
  }
}
