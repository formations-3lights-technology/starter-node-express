import supertest from 'supertest';
import { ActionNonAutorisee } from '@/shared/domain/erreurs/action-non-autorisee';
import { EchecDependance } from '@/shared/domain/erreurs/echec-dependance';
import { ElementEnConflit } from '@/shared/domain/erreurs/element-en-conflit';
import { ElementNonTrouve } from '@/shared/domain/erreurs/element-non-trouve';
import { ErreurDeTraitement } from '@/shared/domain/erreurs/erreur-de-traitement';
import { ErreurDeValidation } from '@/shared/domain/erreurs/erreur-de-validation';
import { NonAutorisation } from '@/shared/domain/erreurs/non-autorisation';
import { ServiceIndisponible } from '@/shared/domain/erreurs/service-indisponible';
import { expect, fakeDependencies, FakeExpressServer } from '@/1-tests/utils';

describe('Integration | Shared | Endpoint erreurs', () => {
  let fakeApi: supertest.SuperTest<supertest.Test>;
  let query;
  const baseUrl = '/v1/ready';

  beforeEach(async () => {
    const dependencies = fakeDependencies();
    query = dependencies.readinessDependenciesContainer.readyCheck;
    const fakeServer = await new FakeExpressServer(dependencies).create();
    fakeApi = fakeServer.api;
  });

  it('Retourne 400', async () => {
    // GIVEN
    query.handle.rejects(new ErreurDeValidation('message'));

    // WHEN
    const response = await fakeApi.get(baseUrl).expect(400);

    // THEN
    expect(response.body).to.eql({
      error: 'BadRequest',
      message: 'message',
      statusCode: 400
    });
  });

  it('Retourne 401', async () => {
    // GIVEN
    query.handle.rejects(new NonAutorisation('message'));

    // WHEN
    const response = await fakeApi.get(baseUrl).expect(401);

    // THEN
    expect(response.body).to.eql({
      error: 'Unauthorized',
      message: 'message',
      statusCode: 401
    });
  });

  it('Retourne 403', async () => {
    // GIVEN
    query.handle.rejects(new ActionNonAutorisee());

    // WHEN
    const response = await fakeApi.get(baseUrl).expect(403);

    // THEN
    expect(response.body).to.eql({
      error: 'Forbidden',
      message: 'Current user does not have good rights',
      statusCode: 403
    });
  });

  it('Retourne 404', async () => {
    // GIVEN
    query.handle.rejects(new ElementNonTrouve('message'));

    // WHEN
    const response = await fakeApi.get(baseUrl).expect(404);

    // THEN
    expect(response.body).to.eql({
      error: 'NotFound',
      message: 'message',
      statusCode: 404
    });
  });

  it('Retourne 409', async () => {
    // GIVEN
    query.handle.rejects(new ElementEnConflit('message'));

    // WHEN
    const response = await fakeApi.get(baseUrl).expect(409);

    // THEN
    expect(response.body).to.eql({
      error: 'Conflict',
      message: 'message',
      statusCode: 409
    });
  });

  it('Retourne 422', async () => {
    // GIVEN
    query.handle.rejects(new ErreurDeTraitement('message'));

    // WHEN
    const response = await fakeApi.get(baseUrl).expect(422);

    // THEN
    expect(response.body).to.eql({
      error: 'UnprocessableEntity',
      message: 'message',
      statusCode: 422
    });
  });

  describe('Retourne 424', () => {
    it('400', async () => {
      // GIVEN
      query.handle.rejects(new EchecDependance(400, 'statut message', 'message'));

      // WHEN
      const response = await fakeApi.get(baseUrl).expect(400);

      // THEN
      expect(response.body).to.eql({
        error: 'BadRequest',
        message: 'message',
        statusCode: 400
      });
    });

    it('404', async () => {
      // GIVEN
      query.handle.rejects(new EchecDependance(404, 'statut message', 'message'));

      // WHEN
      const response = await fakeApi.get(baseUrl).expect(404);

      // THEN
      expect(response.body).to.eql({
        error: 'NotFound',
        message: 'message',
        statusCode: 404
      });
    });

    it('429', async () => {
      // GIVEN
      query.handle.rejects(new EchecDependance(429, 'statut message', 'message'));

      // WHEN
      const response = await fakeApi.get(baseUrl).expect(429);

      // THEN
      expect(response.body).to.eql({
        error: 'TooManyRequests',
        message: 'message',
        statusCode: 429
      });
    });

    it('Par défaut', async () => {
      // GIVEN
      query.handle.rejects(new EchecDependance(499, 'statut message', 'message'));

      // WHEN
      const response = await fakeApi.get(baseUrl).expect(424);

      // THEN
      expect(response.body).to.eql({
        error: 'FailedDependency',
        message: 'message',
        statusCode: 424
      });
    });
  });

  it('Retourne 503', async () => {
    // GIVEN
    query.handle.rejects(new ServiceIndisponible());

    // WHEN
    const response = await fakeApi.get(baseUrl).expect(503);

    // THEN
    expect(response.body).to.eql({
      error: 'ServiceUnavailable',
      message: 'Service unavailable',
      statusCode: 503
    });
  });

  it('Par défaut', async () => {
    // GIVEN
    query.handle.rejects(new Error('message'));

    // WHEN
    const response = await fakeApi.get(baseUrl).expect(500);

    // THEN
    expect(response.body).to.eql({
      error: 'InternalServer',
      message: 'message',
      statusCode: 500
    });
  });
});
