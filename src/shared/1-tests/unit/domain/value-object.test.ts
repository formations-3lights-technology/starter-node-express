import { ValueObject } from '@/shared/domain/value-object';
import { expect } from '@/1-tests/utils';

class ValueObjectTest extends ValueObject<ValueObjectTest, string> {
  constructor(private readonly valueTest: string) {
    super(valueTest);
  }
}

describe('Unit | Shared | Value Object', () => {
  const valueObjectTest = new ValueObjectTest('test');

  it('Récupérer sa valeur', () => {
    expect(valueObjectTest.value).to.eql('test');
  });

  describe('Égalité', () => {
    describe('Est égale', () => {
      it('Par la valeur', () => {
        expect(valueObjectTest.equals('test')).to.be.true();
        expect(valueObjectTest.equals('autre')).to.be.false();
      });

      it("Par l'objet", () => {
        expect(valueObjectTest.is(new ValueObjectTest('test'))).to.be.true();
        expect(valueObjectTest.is(new ValueObjectTest('autre'))).to.be.false();
      });
    });

    describe("N'est pas égale", () => {
      it('Par la valeur', () => {
        expect(valueObjectTest.notEquals('test')).to.be.false();
        expect(valueObjectTest.notEquals('autre')).to.be.true();
      });

      it("Par l'objet", () => {
        expect(valueObjectTest.isNot(new ValueObjectTest('test'))).to.be.false();
        expect(valueObjectTest.isNot(new ValueObjectTest('autre'))).to.be.true();
      });
    });
  });

  describe('Parmi une liste', () => {
    describe('Est dedans', () => {
      it('Par la valeur', () => {
        expect(valueObjectTest.includes(['test', 'test2'])).to.be.true();
        expect(valueObjectTest.includes(['autre', 'autre2'])).to.be.false();
      });

      it("Par l'objet", () => {
        expect(valueObjectTest.among([new ValueObjectTest('test'), new ValueObjectTest('test2')])).to.be.true();
        expect(valueObjectTest.among([new ValueObjectTest('autre'), new ValueObjectTest('autre2')])).to.be.false();
      });
    });

    describe("N'est pas dedans", () => {
      it('Par la valeur', () => {
        expect(valueObjectTest.notIncludes(['test', 'test2'])).to.be.false();
        expect(valueObjectTest.notIncludes(['autre', 'autre2'])).to.be.true();
      });

      it("Par l'objet", () => {
        expect(valueObjectTest.notAmong([new ValueObjectTest('test'), new ValueObjectTest('test2')])).to.be.false();
        expect(valueObjectTest.notAmong([new ValueObjectTest('autre'), new ValueObjectTest('autre2')])).to.be.true();
      });
    });
  });
});
