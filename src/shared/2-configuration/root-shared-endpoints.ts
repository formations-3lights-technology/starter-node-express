import { Express } from 'express';
import { getHealthCheckEndpoint } from '@/shared/infrastructure/endpoints/get-health-check-endpoint';

export const rootSharedEndpoints = (server: Express): void => {
  getHealthCheckEndpoint(server);
};
