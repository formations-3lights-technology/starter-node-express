import {
  ReadinessDependenciesContainer,
  rootReadinessDependenciesContainer
} from '@/readiness/2-configuration/root-readiness-dependencies-container';
import { ApiLogger } from '@/shared/domain/gateways/Api-logger';
import { DateProvider } from '@/shared/domain/gateways/Date-provider';
import { EnvironmentVariables } from '@/shared/domain/gateways/Environment-variables';
import { NodeEnvironmentVariables } from '@/shared/infrastructure/gateways/node-environment-variables';
import { PinoApiLogger } from '@/shared/infrastructure/gateways/pino-api-logger';
import { RealDateProvider } from '@/shared/infrastructure/gateways/real-date-provider';

export type Dependencies = Readonly<{
  environmentVariables: EnvironmentVariables;
  apiLogger: ApiLogger;
  dateProvider: DateProvider;

  readinessDependenciesContainer: ReadinessDependenciesContainer;
}>;

export const dependenciesContainer = (): Dependencies => {
  const environmentVariables = new NodeEnvironmentVariables();
  const apiLogger = new PinoApiLogger(environmentVariables);
  const dateProvider = new RealDateProvider();

  return {
    environmentVariables,
    apiLogger,
    dateProvider,

    readinessDependenciesContainer: rootReadinessDependenciesContainer()
  };
};
