import { Router } from 'express';
import { rootReadinessEndpoints } from '@/readiness/2-configuration/root-readiness-endpoints';
import { Dependencies } from '@/2-configuration/dependencies-container';

export const v1Router = (dependencies: Dependencies): Router => {
  const router = Router();

  rootReadinessEndpoints.unprotected(router, dependencies.readinessDependenciesContainer);

  return router;
};
