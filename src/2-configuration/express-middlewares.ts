import { ErrorRequestHandler, Request, RequestHandler, Response } from 'express';
import { NextFunction } from 'express-serve-static-core';
import { validationResult } from 'express-validator';
import createError from 'http-errors';
import { ActionNonAutorisee } from '@/shared/domain/erreurs/action-non-autorisee';
import { EchecDependance } from '@/shared/domain/erreurs/echec-dependance';
import { ElementEnConflit } from '@/shared/domain/erreurs/element-en-conflit';
import { ElementNonTrouve } from '@/shared/domain/erreurs/element-non-trouve';
import { ErreurDeTraitement } from '@/shared/domain/erreurs/erreur-de-traitement';
import { ErreurDeValidation } from '@/shared/domain/erreurs/erreur-de-validation';
import { LimiteAppel } from '@/shared/domain/erreurs/limite-appel';
import { NonAutorisation } from '@/shared/domain/erreurs/non-autorisation';
import { ServiceIndisponible } from '@/shared/domain/erreurs/service-indisponible';
import { Dependencies } from './dependencies-container';

export interface ExpressMiddlewares {
  preErrorHandling: () => RequestHandler;
  postErrorHandling: (dependencies: Dependencies) => ErrorRequestHandler;
}

export const expressMiddlewares: ExpressMiddlewares = {
  preErrorHandling: (): RequestHandler => {
    return (request: Request, response: Response, next: NextFunction): void => {
      const errors = validationResult(request);
      if (!errors.isEmpty()) {
        throw new ErreurDeValidation(
          errors
            .array()
            .map((e) => `${e.param} - ${e.msg}`)
            .join(' | ')
        );
      }
      next();
    };
  },

  postErrorHandling: (dependencies: Dependencies): ErrorRequestHandler => {
    return (error: any, request: Request, response: Response, _: NextFunction): void => {
      let httpError: createError.HttpError = new createError.InternalServerError(error.message);

      if (error instanceof ErreurDeValidation) {
        httpError = new createError.BadRequest(error.message);
      }
      if (error instanceof NonAutorisation) {
        httpError = new createError.Unauthorized(error.message);
      }
      if (error instanceof ActionNonAutorisee) {
        httpError = new createError.Forbidden(error.message);
      }
      if (error instanceof ElementNonTrouve) {
        httpError = new createError.NotFound(error.message);
      }
      if (error instanceof ElementEnConflit) {
        httpError = new createError.Conflict(error.message);
      }
      if (error instanceof ErreurDeTraitement) {
        httpError = new createError.UnprocessableEntity(error.message);
      }
      if (error instanceof EchecDependance) {
        switch (error.statusCode) {
          case 400:
            httpError = new createError.BadRequest(error.message);
            break;
          case 404:
            httpError = new createError.NotFound(error.message);
            break;
          case 429:
            httpError = new createError.TooManyRequests(error.message);
            break;
          default:
            httpError = new createError.FailedDependency(error.message);
            break;
        }
      }
      if (error instanceof LimiteAppel) {
        httpError = new createError.TooManyRequests(error.message);
      }
      if (error instanceof ServiceIndisponible) {
        httpError = new createError.ServiceUnavailable(error.message);
      }

      const errorBody = {
        statusCode: httpError.statusCode,
        error: httpError.name.replace('Error', ''),
        message: httpError.message
      };

      response.status(httpError.statusCode).send(errorBody);
      log(dependencies, request, response, errorBody);
    };
  }
};

function log(dependencies: Dependencies, request: Request, response: Response, errorBody: any = null) {
  const metadata = logBase(request, response.statusCode);
  const header = `Request completed ${metadata.method} ${metadata.statusCode} ${metadata.path}`;

  if (response.statusCode.toString().startsWith('2')) {
    dependencies.apiLogger.infoWithMetadata(metadata, header);
  }
  if (errorBody) {
    dependencies.apiLogger.errorWithMetadata(metadata, header, errorContent(errorBody));
  }

  function errorContent(errorBody: any) {
    return {
      request: {
        body: response.req.body
      },
      response: {
        body: errorBody
      }
    };
  }
}

function logBase(request: Request, statusCode: number) {
  return {
    correlationId: getStringOrDefault(request.headers['x-correlation-id'], ''),
    method: getStringOrDefault(request.method, ''),
    path: getStringOrDefault(request.path, ''),
    statusCode: getNumberOrDefault(statusCode, 0)
  };
}

function getStringOrDefault<T>(value: T, defaultValue: string): string {
  return String(value) || defaultValue;
}

function getNumberOrDefault<T>(value: T, defaultValue: number): number {
  return Number(value) || defaultValue;
}
